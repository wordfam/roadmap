**Subject to change during development**

Planning, writing, and publishing long-form fiction, specifically in series, is a complex undertaking involving a lot of creativity, expert time management, and summoning the willpower to see the process through.

There are several tools on the market, free and paid, that attempt to make the whole or parts of the process easier to go through. However, sometimes this leaves the author with a swathe of apps to manage everything from tasks, book planning, publishing, and most importantly—writing. Or worse yet, locked into a system they have pay each month to access and use.

Depending on the workflow, managing these tools can add complexity to an already complex workflow, taking precious writing time—or even act as a method of procastination.

Wordfam, as a concept, is the goal to create a robust but flexible system that combines everything an author needs to plan, write, and publish a series of novels while emphasizing the most important part of the process: writing.

# Three Phases
Currently, I have broken this project into three main phases, with each phase focusing on one specific aspect of the authoring process.

Before diving into what each phase entails, I have a short list of rules as I plan each part of this project's development:

1. Every feature, every action must be run through the question of: "How will this help the author write?"
2. Workflows come in many flavors—wordfam must be flexible and easily customizable to accommodate different preferences without becoming encumbered and visually overwhelming or increasing the learning curve of the application.

## Defining the Workflow
In simple terms, writing workflows can be categorized into two main groups: pantsers and plotters. However, in more practical terms this is actually spectrum of workflow preference between discovery writing and planning.

```
[PANTSERS]                                                                             [PLOTTERS]
<----------------------------------------------------------------------------------------------->
```

### Pantsers (Discovery Writing)
Pantsers, or writers who write "by the seat of their pants" may not do any sort of plotting chapters, scenes, and/or beats, especially at the start of a new book project. They prefer to "discover" their book's world and characters through their process of writing. At some point, they may incorporate plotting methodologies, like character sheets or writing out beats of the scene they just sat down to write.

Notable author example: Steven King

### Plotters (Pre-planning)
Plotters are writers who meticulously plan out their books, from top level series arcs to bottom level scene beats. Their world building may be extensive, with large private wikis containing location information, cultures, and robust character sheets. However, plotters may incorporate discovery writing into their workflow. For example, maybe they write from a character's perspective to "get in their head" better.

Notable author example: Brandon Sanderson

## Phase One: Writing, Planning, Tracking
This phase of development covers the core functionality of wordfam. 

### Writing
* Files
  * Markdown format
  * International support, accessibility, i18, voice-to-text
  * File syncing between online and offline
  * File back-up management
  * Version control and edit history
  * Metadata
* Writing Editor
  * Full integration with plot tools for easy reference/look-up (plan -> write)
  * Create world building pages directly from text editor (write -> plan)
  * Word count tracker
  * Words deleted tracker (monitor self-editing while writing)
  * Automatic saving
  * Ability to jump around for non-linear writing (e.g., writing scenes out of order)
  * Auto-update names (copy and replace)
  
### Planning
#### World Building
* Characters
  * Keep track of characters, major and minor, and what novels, chapters, and scenes they appear in
  * Assign character role of protagonist for specific novels
  * Assign fatal flaw/weaknesses to overcome
  * Create networks of character relationships: familial and other (e.g., protagonist <> antagonist)
  * Customizable character templates, including adjustments to core character template
* Locations & Other
  * Glorified wiki that's integrated with the text editor
  * New customizable templates for specific types of content (e.g., Sci-Fi author needs to create locations about planets—"Planet Page", "Alien Culture")

#### Plotting tools
* System to plan from top-level to low-level (e.g., overall series arc to scene beats)
  * Order: Series -> Novel -> Chapter -> Scene -> Beats (can also be planned in reverse)
  * Synopsis for each level
* Themes, symbols
* Draft status:
  * Planning
  * Writing
  * Editing
  * Pre-Publish (ARC)
  * Published
* Due dates/Task list(s)—extrapolate automatically (?)
* Completion %
* Associated characters
* Associated locations (and other world building pages)
* Other tags (?)
* Drill-down methodology for accessing plot tools directly; bubble-up methodology for discovery writers
* Ability to jump around—non-linear planning, rearranging chapters/scenes, access text editor for non-linear writing

### Tracking
* Productivity Statistics
  * Track time spent and word count to show interesting stats—examples could include:
    * Most productive time to write
    * Fastest written (e.g., scene, chapter)
    * Other possible data points of interesting
  * Word Counts
    * Break down by novel, chapter, scene
    * Deleted word count: track self-editing while writing
  * Draft status
    * Planning: tracks how many novels, chapters, scenes, beats have been planned out and generates a task list
    * Writing: rough draft/first draft, pre-send to editor draft 
    * Editing: refinement stage, getting ready to publish
    * Pre-publish: ARC, finalize front/back matter, cover art, book blurbs, formatting for different mediums, send out for proofs, etc.
    * Publish: officially live on sales channels or other avenues of consumption
  * Progress bar: visualize completion status
  * Other types of report data?
* Calendar and due dates:
  * Set due dates for different draft states and for each element of the novel 
  * Calendar view where the author can drag-and-drop "tasks"
  * Smart due date calculator: set time you want the novel done and then auto-generate due dates for each portion of the novel that's been identified through the planning tools

## Phase Two: Collaboration, Editing, and Publishing
Create systems that foster collaboration between co-authors and writing groups. Develop robust systems for editing and collaboration between author(s) and editor(s).


### Group Projects and Co-Authors
Authors can create group series projects and assign members of the group to specific novels (or chapters/scenes).

Majority of work will be building in failsafes and mechanisms to account for potential falling out. Authors should still retain control over the work they have done. If they get kicked out of a group, they should still be able to retain the work they created while in that group.

### Editors
Create a suite of editing tools while accounting for different aspects of editing and integrate within the text editor interface.

* Developmental
* Substantive
* Copy/line-editing
* Proofreading

#### Editing Tools
* Robust commenting abilities.
* Run various types of analytics to figure out things like over-used words and other grammatical red flags.

### Collaboration with non-authors
Authors who have agents, book cover designers, book interior designers, marketers should be able to share their workspace with these individuals in order to foster discussion, create tasks, and follow-up on deliverables.

Payment will have to be handled between them as Wordfam will not have a mechanism to facilitate payments or escrow.

### Educators
Create group management tools specifically for educators to handle their writing classes. Assign students into groups, grading/evaluation, track student progress, run discussions, and post lesson plans/syllabus. Be able to run classes concurrently.

### Publishing Tools
* Export to ebook format—ready to load across all platforms (Amazon, iBooks, etc).
* Export to print format
* Other formats—whatever trad pubs require?
* Query tracking
  * What has sent to who and when and a way to track responses & timelines.
* Author Bookshelf

## Phase Three: Social
Create a front-facing interface that allows authors and readers to interact with each other.

* Disply book statistics such as book progress updates, publishing dates, etc which will automatically update based on progress.
* Places for Authors to write updates/statuses on how things are going... sort of like a micro-blog
* Create alpha/beta/ARC groups

Other social tools for readers and authors.